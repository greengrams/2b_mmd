執行 threejs_2b_mmd.html  
***
# [網頁預覽](https://greengrams.gitlab.io/2b_mmd/threejs_2b_mmd.html)  
操作方法:  
當畫面出現2B表示載入完成，點選左鍵開始撥放  
  
左鍵拖移 - 旋轉攝影機  
滑鼠滾輪 - 遠近控制  
右鍵拖移 - 平移攝影機  
***
使用three.js將mmd的模型與動作載入  
  
並使用TrackballControls控制攝影機  
***
# 材料來源
  
原始模型  
[[MMD] YoRHa 2B DL Updated v1.1](https://www.deviantart.com/arisumatio/art/MMD-YoRHa-2B-DL-Updated-v1-1-670370357)  
使用[PMXEditor](https://blog.xuite.net/alan6716/Vocaloid/46421202-MMD%E6%9C%80%E6%96%B0%E7%89%88%E4%B8%BB%E7%A8%8B%E5%BC%8F%E3%80%81PMDEditor%E6%9C%80%E6%96%B0%E7%89%88%E3%80%81MME%E6%9C%80%E6%96%B0%E7%89%88)  
將'2B clean textures.pmx'移除右手的劍，產生'2B_ct_no_sword.pmx'  
  
[MMD - Gokuraku Jodo (Motion Data + Camera + Wav download)](https://www.youtube.com/watch?v=CzmXDgJIrYk)  
下方所提供的  
[Motion Data + Camera + Wav download](http://www.mediafire.com/file/la3f6xubtvww327/Gokuraku+Jodo.7z)  
  
將 Gokuraku Jodo.vmd 動作擋 以 MMD 開啟後重新儲存為 Gokuraku Jodo3.vmd  
使用 MMDLoader 讀取 Gokuraku Jodo.vmd 會有問題  